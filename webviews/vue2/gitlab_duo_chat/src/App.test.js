import { nextTick } from 'vue';
import { shallowMount } from '@vue/test-utils';
import { GlDuoChat } from '@gitlab/ui';
import App from './App.vue';

describe('Duo Chat Vue app', () => {
  let wrapper;
  let vsCodeApi;

  const findDuoChat = () => wrapper.findComponent(GlDuoChat);

  const createComponent = () => {
    wrapper = shallowMount(App);
  };

  beforeEach(() => {
    vsCodeApi = acquireVsCodeApi();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('application bootstrapping', () => {
    it('posts the `appReady` message when created', () => {
      expect(vsCodeApi.postMessage).not.toHaveBeenCalled();
      createComponent();
      expect(vsCodeApi.postMessage).toHaveBeenCalledWith({
        command: 'appReady',
      });
    });
  });

  describe('GlDuoChat integration', () => {
    it('renders the Duo Chat component', () => {
      createComponent();
      expect(findDuoChat().exists()).toBe(true);
    });

    it('correctly sets the props on Duo Chat', async () => {
      const chatMessages = [
        {
          content: 'Foo',
          role: 'user',
        },
        {
          content: 'Bar',
          role: 'assistant',
        },
      ];

      createComponent();
      expect(findDuoChat().props('messages')).toEqual([]);

      chatMessages.forEach(record => {
        window.dispatchEvent(
          new MessageEvent('message', {
            data: {
              eventType: 'newRecord',
              record,
            },
          }),
        );
      });
      await nextTick();

      expect(findDuoChat().props('messages')).toEqual(chatMessages);
    });

    it('sends regular prompt with the `newPrompt` event type', async () => {
      const question = 'What is GitLab?';
      createComponent();

      findDuoChat().vm.$emit('send-chat-prompt', question);
      await nextTick();

      expect(vsCodeApi.postMessage).toHaveBeenCalledWith({
        eventType: 'newPrompt',
        record: {
          content: question,
        },
      });
    });

    it.each([['/clean'], ['/clear']])(
      'sends clean command with the `cleanChat` event type',
      async cleanChatMessage => {
        createComponent();

        findDuoChat().vm.$emit('send-chat-prompt', cleanChatMessage);

        await nextTick();

        expect(vsCodeApi.postMessage).toHaveBeenCalledWith({
          eventType: 'cleanChat',
          record: {
            content: cleanChatMessage,
          },
        });
      },
    );
  });
});
