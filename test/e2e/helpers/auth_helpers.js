import { browser } from '@wdio/globals';
import { waitForPromptTitleToContain } from './command_palette_helpers.js';
import { dismissAllNotifications, waitForNotification } from './notification_helpers.js';

export const selectPatAuthenticationAndOpenTokenInput = async () => {
  await dismissAllNotifications();
  const workbench = await browser.getWorkbench();
  const prompt = await workbench.executeCommand('GitLab: Authenticate');

  await waitForPromptTitleToContain(prompt, 'Select GitLab instance');
  await prompt.selectQuickPick('https://gitlab.com');

  await waitForPromptTitleToContain(prompt, 'Select authentication method');
  await prompt.selectQuickPick('Personal Access Token');

  await waitForPromptTitleToContain(prompt, 'Do you want to create a new token?');
  await prompt.selectQuickPick('Enter an existing token');

  await expect(prompt.input$).toHaveAttr('type', 'password', {
    message: 'Input field is not masked for passwords.',
  });

  return prompt;
};

/**
 * Completes authorization with environment variable `TEST_GITLAB_TOKEN` for the GitLab extension
 *
 * @async
 * @returns {Promise<void>}
 */
export const completeAuth = async () => {
  if (!process.env.TEST_GITLAB_TOKEN) {
    throw new Error('TEST_GITLAB_TOKEN environment variable is not set!');
  }

  const prompt = await selectPatAuthenticationAndOpenTokenInput();

  await prompt.setText(process.env.TEST_GITLAB_TOKEN);
  await prompt.confirm();

  await waitForNotification('Added GitLab account for user');
};
