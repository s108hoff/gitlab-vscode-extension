import * as vscode from 'vscode';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { createActiveTextEditorChangeTrigger } from '../../test_utils/vscode_fakes';
import { AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES } from '../constants';
import { getAIAssistedCodeSuggestionsLanguages } from '../../utils/extension_configuration';
import { SupportedLanguagePolicy, UNSUPPORTED_LANGUAGE } from './supported_language_policy';

jest.mock('../../utils/extension_configuration');

describe('SupportedLanguagePolicy', () => {
  let policy: SupportedLanguagePolicy;
  const triggerActiveTextEditorChange = createActiveTextEditorChangeTrigger();

  const unsupportedLanguages = ['diff', 'haml', 'lua'];
  beforeEach(() => {
    policy = new SupportedLanguagePolicy();
    jest
      .mocked(getAIAssistedCodeSuggestionsLanguages)
      .mockReturnValue(AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES);
  });

  it('has UNSUPPORTED_LANGUAGE state', () => {
    expect(policy.state).toBe(UNSUPPORTED_LANGUAGE);
  });

  it('is not engaged when language is supported', async () => {
    AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES.forEach(async languageId => {
      await triggerActiveTextEditorChange(
        createFakePartial<vscode.TextEditor>({ document: { languageId } }),
      );

      expect(policy.engaged).toBe(false);
    });
  });

  it('is engaged when language is NOT supported', async () => {
    unsupportedLanguages.forEach(async languageId => {
      await triggerActiveTextEditorChange(
        createFakePartial<vscode.TextEditor>({ document: { languageId } }),
      );

      expect(policy.engaged).toBe(true);
    });
  });

  it('fires `onEngagedChange` event when the engaged changes', async () => {
    const listener = jest.fn();
    policy.onEngagedChange(listener);

    expect(listener).not.toHaveBeenCalled();

    await triggerActiveTextEditorChange(
      createFakePartial<vscode.TextEditor>({ document: { languageId: 'javascript' } }),
    );

    expect(listener).toHaveBeenCalledWith(false);

    await triggerActiveTextEditorChange(
      createFakePartial<vscode.TextEditor>({ document: { languageId: 'json' } }),
    );

    expect(listener).toHaveBeenCalledWith(true);
  });
});
