import * as vscode from 'vscode';
import { StatusBarItemUI, createStatusBarItem } from '../utils/status_bar_item';
import { SHOW_QUICK_PICK_MENU } from '../duo_quick_pick/commands/show_quick_pick_menu';
import {
  CodeSuggestionsStateManager,
  VisibleCodeSuggestionsState,
} from './code_suggestions_state_manager';

export const CODE_SUGGESTIONS_STATUSES: Record<VisibleCodeSuggestionsState, StatusBarItemUI> = {
  [VisibleCodeSuggestionsState.LOADING]: {
    text: '$(gitlab-code-suggestions-loading)',
    tooltip: 'Code suggestion is loading...',
  },
  [VisibleCodeSuggestionsState.DISABLED_VIA_SETTINGS]: {
    text: '$(gitlab-code-suggestions-disabled)',
    tooltip: 'Code suggestions are disabled',
  },
  [VisibleCodeSuggestionsState.READY]: {
    text: '$(gitlab-code-suggestions-enabled)',
    tooltip: 'Code suggestions are enabled',
  },
  [VisibleCodeSuggestionsState.ERROR]: {
    text: '$(gitlab-code-suggestions-error)',
    tooltip: 'There was an error fetching code suggestions. See extension logs',
  },
  [VisibleCodeSuggestionsState.UNSUPPORTED_LANGUAGE]: {
    text: '$(gitlab-code-suggestions-disabled)',
    tooltip: 'Code suggestions are not supported for this language',
  },
  [VisibleCodeSuggestionsState.DISABLED_BY_USER]: {
    text: '$(gitlab-code-suggestions-disabled)',
    tooltip: 'Code suggestions are disabled per user request. Click to enable',
  },
  [VisibleCodeSuggestionsState.NO_ACCOUNT]: {
    text: '$(gitlab-code-suggestions-disabled)',
    tooltip: 'Code suggestions are disabled because there is no user account',
  },
  [VisibleCodeSuggestionsState.NO_LICENSE]: {
    text: '$(gitlab-code-suggestions-disabled)',
    tooltip:
      'Code Suggestions is now a paid feature, part of Duo Pro. Contact your GitLab administrator to upgrade',
  },
  [VisibleCodeSuggestionsState.DISABLED_BY_PROJECT]: {
    text: '$(gitlab-code-suggestions-disabled)',
    tooltip: 'Code suggestions are disabled for this project',
  },
  [VisibleCodeSuggestionsState.UNSUPPORTED_GITLAB_VERSION]: {
    text: '$(gitlab-code-suggestions-disabled)',
    tooltip: 'GitLab Duo Code Suggestions requires GitLab version 16.8 or later.',
  },
};

export class CodeSuggestionsStatusBarItem {
  codeSuggestionsStatusBarItem?: vscode.StatusBarItem;

  #codeSuggestionsStateSubscription?: vscode.Disposable;

  updateCodeSuggestionsItem(state: VisibleCodeSuggestionsState) {
    if (!this.codeSuggestionsStatusBarItem) return;

    const newUiState = CODE_SUGGESTIONS_STATUSES[state];

    this.codeSuggestionsStatusBarItem.text = newUiState.text;
    this.codeSuggestionsStatusBarItem.tooltip = newUiState.tooltip;
  }

  constructor(state: CodeSuggestionsStateManager) {
    this.codeSuggestionsStatusBarItem = createCodeSuggestionStatusBarItem();
    this.updateCodeSuggestionsItem(state.getVisibleState());
    this.#codeSuggestionsStateSubscription = state.onDidChangeVisibleState(e =>
      this.updateCodeSuggestionsItem(e),
    );
  }

  dispose(): void {
    this.#codeSuggestionsStateSubscription?.dispose();
  }
}

function createCodeSuggestionStatusBarItem() {
  return createStatusBarItem({
    priority: 3,
    id: 'gl.status.code_suggestions',
    name: 'GitLab Workflow: Code Suggestions',
    initialText: '$(gitlab-code-suggestions-disabled)',
    command: SHOW_QUICK_PICK_MENU,
  });
}
