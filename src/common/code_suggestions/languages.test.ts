import * as packageJson from '../../../package.json';
import { AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES } from './constants';

const key = 'gitlab.aiAssistedCodeSuggestions.enabledSupportedLanguages';
const schema = packageJson.contributes.configuration.properties[key];

describe('AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES', () => {
  it('should match enabledSupportedLanguages setting schema', () => {
    // NOTE: Some languages (e.g., html, css) are not listed as supported by
    // the language server, but are by the VS Code extension. See these merge_requests:
    // - https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/1554
    // - https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/1536
    expect(AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES).toEqual(
      expect.arrayContaining(Object.keys(schema.properties)),
    );
  });
});

describe('schema consistency', () => {
  it('should have a complete default value', () => {
    expect(Object.keys(schema.properties)).toEqual(Object.keys(schema.default));
  });
});
