import vscode from 'vscode';
import { CodeSuggestionsStateManager } from '../../code_suggestions/code_suggestions_state_manager';
import { COMMAND_TOGGLE_CODE_SUGGESTIONS } from '../../code_suggestions/commands/toggle';
import { COMMAND_TOGGLE_CODE_SUGGESTIONS_FOR_LANGUAGE } from '../../code_suggestions/commands/toggle_language';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import * as utils from '../utils';
import { showDuoQuickPickMenu } from './show_quick_pick_menu';

describe('show duo quick pick menu command', () => {
  let mockStateManager: CodeSuggestionsStateManager;
  let mockQuickPick: vscode.QuickPick<vscode.QuickPickItem>;

  const setupTest = (isDisabledByUser = false) => {
    mockStateManager = createFakePartial<CodeSuggestionsStateManager>({
      isDisabledByUser: jest.fn().mockReturnValue(isDisabledByUser),
    });

    mockQuickPick = createFakePartial<vscode.QuickPick<vscode.QuickPickItem>>({
      items: [],
      onDidChangeSelection: jest.fn(),
      onDidHide: jest.fn(),
      show: jest.fn(),
      hide: jest.fn(),
      dispose: jest.fn(),
    });

    (vscode.window.createQuickPick as jest.Mock).mockReturnValue(mockQuickPick);
  };

  beforeEach(async () => {
    setupTest();
    await showDuoQuickPickMenu({ stateManager: mockStateManager });
  });

  afterEach(() => jest.clearAllMocks());

  it('should create a QuickPick with correct items', async () => {
    const separator = { label: '', kind: vscode.QuickPickItemKind.Separator };
    const items = [
      {
        label: '$(gitlab-code-suggestions-enabled) GitLab Duo Code Suggestions: Enabled',
        description: 'Code completion + generation',
      },
      separator,
      { label: 'Disable Code Suggestions' },
      separator,
      { label: 'Documentation' },
      { label: 'GitLab Forum', description: 'Help and feedback' },
    ];
    expect(vscode.window.createQuickPick).toHaveBeenCalled();
    expect(mockQuickPick.items).toEqual(items);
    expect(mockQuickPick.show).toHaveBeenCalled();
  });

  it('should handle selection of the Code Suggestions status item', async () => {
    const mockActiveEditor = createFakePartial<vscode.TextEditor>({
      document: createFakePartial<vscode.TextDocument>({
        getText: jest.fn().mockReturnValue('editorText'),
      }),
    });

    vscode.window.activeTextEditor = mockActiveEditor;

    const selectionCallback = (mockQuickPick.onDidChangeSelection as jest.Mock).mock.calls[0][0];
    await selectionCallback([mockQuickPick.items[0]]);

    expect(vscode.window.showTextDocument).toHaveBeenCalledWith(mockActiveEditor.document);
    expect(mockQuickPick.hide).toHaveBeenCalled();
  });

  it('should handle selection of Code Suggestions toggle item', async () => {
    const selectionCallback = (mockQuickPick.onDidChangeSelection as jest.Mock).mock.calls[0][0];
    await selectionCallback([mockQuickPick.items[2]]);

    expect(vscode.commands.executeCommand).toHaveBeenCalledWith(COMMAND_TOGGLE_CODE_SUGGESTIONS);
    expect(mockQuickPick.hide).toHaveBeenCalled();
  });

  it('should handle selection of the Documentation item', async () => {
    const documentationUrl = 'https://docs.gitlab.com/ee/user/gitlab_duo/';
    const selectionCallback = (mockQuickPick.onDidChangeSelection as jest.Mock).mock.calls[0][0];
    await selectionCallback([mockQuickPick.items[4]]);

    expect(vscode.env.openExternal).toHaveBeenCalledWith(vscode.Uri.parse(documentationUrl));
  });

  it('should handle selection of the GitLab Forum item', async () => {
    const forumUrl = 'https://forum.gitlab.com/c/gitlab-duo/52';
    const selectionCallback = (mockQuickPick.onDidChangeSelection as jest.Mock).mock.calls[0][0];
    await selectionCallback([mockQuickPick.items[5]]);

    expect(vscode.env.openExternal).toHaveBeenCalledWith(vscode.Uri.parse(forumUrl));
  });

  it('should dispose QuickPick on hide', async () => {
    const hideCallback = (mockQuickPick.onDidHide as jest.Mock).mock.calls[0][0];
    hideCallback();

    expect(mockQuickPick.dispose).toHaveBeenCalled();
  });

  it('should show information message when code suggestions is disabled', async () => {
    setupTest(true);
    await showDuoQuickPickMenu({ stateManager: mockStateManager });

    const selectionCallback = (mockQuickPick.onDidChangeSelection as jest.Mock).mock.calls[0][0];
    await selectionCallback([mockQuickPick.items[0]]);

    expect(vscode.window.showInformationMessage).toHaveBeenCalledWith(
      'GitLab Duo Code Suggestions is currently disabled.',
    );
  });

  it('should show information message when no active editor', async () => {
    vscode.window.activeTextEditor = undefined;

    const selectionCallback = (mockQuickPick.onDidChangeSelection as jest.Mock).mock.calls[0][0];
    await selectionCallback([mockQuickPick.items[0]]);

    expect(vscode.window.showInformationMessage).toHaveBeenCalledWith(
      'No active editor. Open a file to use Duo Code Suggestions.',
    );
  });

  describe('code suggestions language toggle', () => {
    const toggleItemMock = { label: 'Enable Code Suggestions for javascript' };

    beforeEach(async () => {
      jest.spyOn(utils, 'generateCodeSuggestionsLangToggleItem').mockReturnValue(toggleItemMock);

      await showDuoQuickPickMenu({ stateManager: mockStateManager });
    });

    it('should add a language toggle at index 2 when CS are enabled globally', () => {
      expect(mockQuickPick.items[2]).toEqual(toggleItemMock);
    });

    it('should handle selection of the language toggle', async () => {
      const selectionCallback = jest.mocked(mockQuickPick.onDidChangeSelection).mock.calls[0][0];
      await selectionCallback([mockQuickPick.items[2]]);

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        COMMAND_TOGGLE_CODE_SUGGESTIONS_FOR_LANGUAGE,
      );
      expect(mockQuickPick.hide).toHaveBeenCalled();
    });

    it('should not add a language toggle at index 2 when item is not generated', async () => {
      jest.spyOn(utils, 'generateCodeSuggestionsLangToggleItem').mockReturnValue(undefined);
      await showDuoQuickPickMenu({ stateManager: mockStateManager });

      expect(mockQuickPick.items[2]).not.toEqual(toggleItemMock);
    });
  });
});
