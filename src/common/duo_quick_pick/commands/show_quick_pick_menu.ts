import vscode from 'vscode';
import {
  generateCodeSuggestionsStatusItem,
  generateCodeSuggestionsToggleItem,
  generateCodeSuggestionsLangToggleItem,
  generateQuickPickItem,
} from '../utils';
import {
  CODE_SUGGESTIONS_DISABLED_NOTIFICATION,
  NO_ACTIVE_EDITOR_NOTIFICATION,
  DOCUMENTATION,
  DOCUMENTATION_URL,
  GITLAB_FORUM,
  GITLAB_FORUM_DESCRIPTION,
  GITLAB_FORUM_URL,
} from '../constants';
import { CodeSuggestionsStateManager } from '../../code_suggestions/code_suggestions_state_manager';
import { COMMAND_TOGGLE_CODE_SUGGESTIONS } from '../../code_suggestions/commands/toggle';
import { COMMAND_TOGGLE_CODE_SUGGESTIONS_FOR_LANGUAGE } from '../../code_suggestions/commands/toggle_language';

export const SHOW_QUICK_PICK_MENU = 'gl.showDuoQuickPickMenu';

const focusActiveEditor = (codeSuggestionsEnabled: boolean) => {
  if (!codeSuggestionsEnabled)
    return vscode.window.showInformationMessage(CODE_SUGGESTIONS_DISABLED_NOTIFICATION);

  const activeEditor = vscode.window.activeTextEditor;
  if (!activeEditor) return vscode.window.showInformationMessage(NO_ACTIVE_EDITOR_NOTIFICATION);

  return vscode.window.showTextDocument(activeEditor.document);
};

const toggleCodeSuggestions = () => vscode.commands.executeCommand(COMMAND_TOGGLE_CODE_SUGGESTIONS);
const toggleCodeSuggestionsForLanguage = () =>
  vscode.commands.executeCommand(COMMAND_TOGGLE_CODE_SUGGESTIONS_FOR_LANGUAGE);
const openDocumentation = () => vscode.env.openExternal(vscode.Uri.parse(DOCUMENTATION_URL));
const openGitLabForum = () => vscode.env.openExternal(vscode.Uri.parse(GITLAB_FORUM_URL));

export const showDuoQuickPickMenu = async ({
  stateManager,
}: {
  stateManager: CodeSuggestionsStateManager;
}) => {
  const codeSuggestionsEnabled = !stateManager.isDisabledByUser();
  const separatorItem = { label: '', kind: vscode.QuickPickItemKind.Separator };
  const codeSuggestionsStatusItem = generateCodeSuggestionsStatusItem(codeSuggestionsEnabled);
  const codeSuggestionsToggleItem = generateCodeSuggestionsToggleItem(codeSuggestionsEnabled);
  const codeSuggestionsLanguageToggleItem =
    generateCodeSuggestionsLangToggleItem(codeSuggestionsEnabled);
  const documentationItem = generateQuickPickItem(DOCUMENTATION);
  const gitlabForumItem = generateQuickPickItem(GITLAB_FORUM, GITLAB_FORUM_DESCRIPTION);
  const quickPick = vscode.window.createQuickPick();
  const quickPickItems = [
    codeSuggestionsStatusItem,
    separatorItem,
    codeSuggestionsToggleItem,
    separatorItem,
    documentationItem,
    gitlabForumItem,
  ];

  if (codeSuggestionsLanguageToggleItem) {
    quickPickItems.splice(2, 0, codeSuggestionsLanguageToggleItem);
  }

  quickPick.items = quickPickItems;

  quickPick.onDidChangeSelection(async selection => {
    switch (selection[0]) {
      case codeSuggestionsStatusItem:
        await focusActiveEditor(codeSuggestionsEnabled);
        break;
      case codeSuggestionsToggleItem:
        await toggleCodeSuggestions();
        break;
      case codeSuggestionsLanguageToggleItem:
        await toggleCodeSuggestionsForLanguage();
        break;
      case documentationItem:
        await openDocumentation();
        break;
      case gitlabForumItem:
        await openGitLabForum();
        break;
      default:
        break;
    }

    quickPick.hide();
  });

  quickPick.onDidHide(() => quickPick.dispose());
  quickPick.show();
};
