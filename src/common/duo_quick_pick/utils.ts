import vscode from 'vscode';
import { getAiAssistedCodeSuggestionsConfiguration } from '../utils/extension_configuration';
import {
  CODE_SUGGESTIONS_ENABLED,
  CODE_SUGGESTIONS_DISABLED,
  CODE_SUGGESTIONS_DESCRIPTION,
  ENABLE_CODE_SUGGESTIONS,
  DISABLE_CODE_SUGGESTIONS,
} from './constants';

export const generateQuickPickItem = (
  label: string,
  description?: string,
): vscode.QuickPickItem => ({
  label,
  description,
});

export const generateCodeSuggestionsStatusItem = (enabled: boolean): vscode.QuickPickItem => {
  const label = enabled ? CODE_SUGGESTIONS_ENABLED : CODE_SUGGESTIONS_DISABLED;
  return generateQuickPickItem(label, CODE_SUGGESTIONS_DESCRIPTION);
};

export const generateCodeSuggestionsToggleItem = (enabled: boolean): vscode.QuickPickItem => {
  const label = enabled ? DISABLE_CODE_SUGGESTIONS : ENABLE_CODE_SUGGESTIONS;
  return generateQuickPickItem(label);
};

const getCurrentFileLanguage = (): string | undefined => {
  const activeEditor = vscode.window.activeTextEditor;
  return activeEditor ? activeEditor.document.languageId : undefined;
};

export const generateCodeSuggestionsLangToggleItem = (
  globallyEnabled: boolean,
): vscode.QuickPickItem | undefined => {
  let quickPickItem;
  const { enabledSupportedLanguages, additionalLanguages } =
    getAiAssistedCodeSuggestionsConfiguration();
  const language = getCurrentFileLanguage();

  if (globallyEnabled && language) {
    const languageEnabled =
      enabledSupportedLanguages[language] || additionalLanguages.includes(language);
    const action = languageEnabled ? DISABLE_CODE_SUGGESTIONS : ENABLE_CODE_SUGGESTIONS;
    const label = `${action} for ${language}`;

    quickPickItem = generateQuickPickItem(label);
  }

  return quickPickItem;
};
