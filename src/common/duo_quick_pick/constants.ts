export const CODE_SUGGESTIONS_ENABLED =
  '$(gitlab-code-suggestions-enabled) GitLab Duo Code Suggestions: Enabled';
export const CODE_SUGGESTIONS_DISABLED =
  '$(gitlab-code-suggestions-disabled) GitLab Duo Code Suggestions: Disabled';
export const CODE_SUGGESTIONS_DESCRIPTION = 'Code completion + generation';
export const ENABLE_CODE_SUGGESTIONS = 'Enable Code Suggestions';
export const DISABLE_CODE_SUGGESTIONS = 'Disable Code Suggestions';
export const DOCUMENTATION = 'Documentation';
export const GITLAB_FORUM = 'GitLab Forum';
export const GITLAB_FORUM_DESCRIPTION = 'Help and feedback';

export const DOCUMENTATION_URL = 'https://docs.gitlab.com/ee/user/gitlab_duo/';
export const GITLAB_FORUM_URL = 'https://forum.gitlab.com/c/gitlab-duo/52';

export const NO_ACTIVE_EDITOR_NOTIFICATION =
  'No active editor. Open a file to use Duo Code Suggestions.';
export const CODE_SUGGESTIONS_DISABLED_NOTIFICATION =
  'GitLab Duo Code Suggestions is currently disabled.';
