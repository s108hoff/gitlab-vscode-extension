import vscode from 'vscode';
import {
  getAiAssistedCodeSuggestionsConfiguration,
  AiAssistedCodeSuggestionsConfiguration,
} from '../utils/extension_configuration';
import { createFakePartial } from '../test_utils/create_fake_partial';
import {
  generateCodeSuggestionsStatusItem,
  generateCodeSuggestionsToggleItem,
  generateCodeSuggestionsLangToggleItem,
  generateQuickPickItem,
} from './utils';
import {
  CODE_SUGGESTIONS_ENABLED,
  CODE_SUGGESTIONS_DISABLED,
  CODE_SUGGESTIONS_DESCRIPTION,
  ENABLE_CODE_SUGGESTIONS,
  DISABLE_CODE_SUGGESTIONS,
} from './constants';

jest.mock('../utils/extension_configuration');

describe('Quick Pick Utils', () => {
  const label = 'some label';

  describe('generateQuickPickItem', () => {
    it('should generate a QuickPickItem with a label if only a label is provided', () => {
      const result = generateQuickPickItem(label);
      expect(result).toEqual({ label });
    });

    it('should generate a QuickPickItem with a label and a description', () => {
      const description = 'some description';
      const result = generateQuickPickItem(label, description);
      expect(result).toEqual({ label, description });
    });
  });

  describe('generateCodeSuggestionsStatusItem', () => {
    it('should generate a QuickPickItem for enabled code suggestions', () => {
      const result = generateCodeSuggestionsStatusItem(true);
      expect(result).toEqual({
        label: CODE_SUGGESTIONS_ENABLED,
        description: CODE_SUGGESTIONS_DESCRIPTION,
      });
    });

    it('should generate a QuickPickItem for disabled code suggestions', () => {
      const result = generateCodeSuggestionsStatusItem(false);
      expect(result).toEqual({
        label: CODE_SUGGESTIONS_DISABLED,
        description: CODE_SUGGESTIONS_DESCRIPTION,
      });
    });
  });

  describe('generateCodeSuggestionsToggleItem', () => {
    it('should generate a QuickPickItem to disable code suggestions when enabled', () => {
      const result = generateCodeSuggestionsToggleItem(true);
      expect(result).toEqual({ label: DISABLE_CODE_SUGGESTIONS });
    });

    it('should generate a QuickPickItem to enable code suggestions when disabled', () => {
      const result = generateCodeSuggestionsToggleItem(false);
      expect(result).toEqual({ label: ENABLE_CODE_SUGGESTIONS });
    });
  });

  describe('generateCodeSuggestionsLangToggleItem', () => {
    beforeEach(() => {
      jest.mocked(getAiAssistedCodeSuggestionsConfiguration).mockReturnValue(
        createFakePartial<AiAssistedCodeSuggestionsConfiguration>({
          enabledSupportedLanguages: { javascript: false },
          additionalLanguages: [],
        }),
      );

      vscode.window.activeTextEditor = createFakePartial<vscode.TextEditor>({
        document: createFakePartial<vscode.TextDocument>({
          languageId: 'javascript',
        }),
      });
    });

    it('should return undefined when code suggestions is globally disabled', () => {
      const result = generateCodeSuggestionsLangToggleItem(false);
      expect(result).toBeUndefined();
    });

    it('should return undefined when no active editor is open', () => {
      vscode.window.activeTextEditor = undefined;

      const result = generateCodeSuggestionsLangToggleItem(true);
      expect(result).toBeUndefined();
    });

    it('should return "Enable" item when language is not enabled', () => {
      const result = generateCodeSuggestionsLangToggleItem(true);
      expect(result).toEqual({
        label: `${ENABLE_CODE_SUGGESTIONS} for javascript`,
      });
    });

    it('should return "Disable" item when language is enabled', () => {
      jest.mocked(getAiAssistedCodeSuggestionsConfiguration).mockReturnValue(
        createFakePartial<AiAssistedCodeSuggestionsConfiguration>({
          enabledSupportedLanguages: { javascript: true },
          additionalLanguages: [],
        }),
      );

      const result = generateCodeSuggestionsLangToggleItem(true);
      expect(result).toEqual({
        label: `${DISABLE_CODE_SUGGESTIONS} for javascript`,
      });
    });

    it('should return "Disable" item when language is in additionalLanguages', () => {
      jest.mocked(getAiAssistedCodeSuggestionsConfiguration).mockReturnValue(
        createFakePartial<AiAssistedCodeSuggestionsConfiguration>({
          enabledSupportedLanguages: {},
          additionalLanguages: ['javascript'],
        }),
      );

      const result = generateCodeSuggestionsLangToggleItem(true);
      expect(result).toEqual({
        label: `${DISABLE_CODE_SUGGESTIONS} for javascript`,
      });
    });
  });
});
