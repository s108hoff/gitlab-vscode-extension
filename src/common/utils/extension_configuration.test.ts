import * as vscode from 'vscode';
import { createFakePartial } from '../test_utils/create_fake_partial';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES,
  AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE,
} from '../code_suggestions/constants';
import { createFakeWorkspaceConfiguration } from '../test_utils/vscode_fakes';
import {
  getAIAssistedCodeSuggestionsLanguages,
  setAiAssistedCodeSuggestionsConfiguration,
  parseDisabledSupportedLanguages,
  getAiAssistedCodeSuggestionsConfiguration,
} from './extension_configuration';

describe('utils/extension_configuration', () => {
  describe('setAiAssistedCodeSuggestionsConfiguration', () => {
    let mockAiConfig: vscode.WorkspaceConfiguration;

    beforeEach(() => {
      mockAiConfig = createFakePartial<vscode.WorkspaceConfiguration>({
        update: jest.fn().mockResolvedValue(undefined),
        inspect: jest.fn().mockReturnValue(undefined),
      });

      jest.mocked(vscode.workspace.getConfiguration).mockReturnValue(mockAiConfig);
    });

    it('requests AI config', async () => {
      expect(vscode.workspace.getConfiguration).not.toHaveBeenCalled();

      await setAiAssistedCodeSuggestionsConfiguration({
        enabled: true,
      });

      expect(vscode.workspace.getConfiguration).toHaveBeenCalledTimes(1);
      expect(vscode.workspace.getConfiguration).toHaveBeenCalledWith(
        AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE,
      );
    });

    it.each`
      config                                          | mockInspect                  | expectedUpdate
      ${{ enabled: true }}                            | ${undefined}                 | ${[['enabled', true, vscode.ConfigurationTarget.Global]]}
      ${{ enabled: false }}                           | ${{ workspaceValue: false }} | ${[['enabled', false, vscode.ConfigurationTarget.Workspace]]}
      ${{ enabled: true, preferredAccount: 'lorem' }} | ${{ workspaceValue: false }} | ${[['enabled', true, vscode.ConfigurationTarget.Workspace], ['preferredAccount', 'lorem', vscode.ConfigurationTarget.Workspace]]}
      ${{ additionalLanguages: ['foo'] }}             | ${undefined}                 | ${[['additionalLanguages', ['foo'], vscode.ConfigurationTarget.Global]]}
      ${{ enabledSupportedLanguages: { c: false } }}  | ${undefined}                 | ${[['enabledSupportedLanguages', { c: false }, vscode.ConfigurationTarget.Global]]}
      ${{ enabledSupportedLanguages: undefined }}     | ${undefined}                 | ${[['enabledSupportedLanguages', undefined, vscode.ConfigurationTarget.Global]]}
    `(
      'with config=$config and inspect=$mockInspect, should update',
      async ({ config, mockInspect, expectedUpdate }) => {
        jest.mocked(mockAiConfig.inspect).mockReturnValue(mockInspect);
        expect(mockAiConfig.update).not.toHaveBeenCalled();

        await setAiAssistedCodeSuggestionsConfiguration(config);

        expect(jest.mocked(mockAiConfig.update).mock.calls).toEqual(expectedUpdate);
      },
    );
  });

  describe('getAiAssistedCodeSuggestionsConfiguration', () => {
    describe('given valid values', () => {
      beforeEach(() => {
        jest.mocked(vscode.workspace.getConfiguration).mockReturnValueOnce(
          createFakeWorkspaceConfiguration({
            enabled: true,
            preferredAccount: 'a_user',
            suggestionsCache: undefined,
            additionalLanguages: ['foo'],
            enabledSupportedLanguages: { c: true, python: false },
            openTabsContext: true,
          }),
        );
      });

      it('return the values', () => {
        expect(getAiAssistedCodeSuggestionsConfiguration()).toEqual({
          enabled: true,
          preferredAccount: 'a_user',
          suggestionsCache: undefined,
          additionalLanguages: ['foo'],
          enabledSupportedLanguages: { c: true, python: false },
          openTabsContext: true,
        });
      });
    });

    describe('given invalid values', () => {
      beforeEach(() => {
        jest.mocked(vscode.workspace.getConfiguration).mockReturnValueOnce(
          createFakeWorkspaceConfiguration({
            enabled: 'true',
            preferredAccount: 42,
            suggestionsCache: undefined,
            additionalLanguages: 'foo,bar',
            enabledSupportedLanguages: { c: 'true', python: null },
            openTabsContext: 'false',
          }),
        );
      });

      it('returns sensible default values', () => {
        expect(getAiAssistedCodeSuggestionsConfiguration()).toEqual({
          enabled: true,
          preferredAccount: undefined,
          suggestionsCache: undefined,
          additionalLanguages: [],
          enabledSupportedLanguages: {},
          openTabsContext: true,
        });
      });
    });
  });

  describe('parseDisabledSupportedLanguages', () => {
    it('should returns empty lists given empty configuration', () => {
      expect(parseDisabledSupportedLanguages({})).toEqual([]);
    });

    it('should return lists representing configured languages', () => {
      expect(parseDisabledSupportedLanguages({ cpp: true, python: false })).toEqual(['python']);
    });
  });

  describe('getAIAssistedCodeSuggestionsLanguages', () => {
    it('should return AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES with user-configured languages', () => {
      const userConfiguredLanguages = ['foo', 'bar'];

      jest.mocked(vscode.workspace.getConfiguration).mockReturnValueOnce(
        createFakeWorkspaceConfiguration({
          additionalLanguages: userConfiguredLanguages,
        }),
      );

      expect(getAIAssistedCodeSuggestionsLanguages()).toEqual([
        ...AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES,
        ...userConfiguredLanguages,
      ]);
    });

    it('should return default languages when user settings are malformed', () => {
      jest.mocked(vscode.workspace.getConfiguration).mockReturnValueOnce(
        createFakeWorkspaceConfiguration({
          additionalLanguages: false,
          enabledSupportedLanguages: null,
        }),
      );

      expect(getAIAssistedCodeSuggestionsLanguages()).toEqual(
        AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES,
      );
    });

    it('should not return disabled lanuages', () => {
      jest.mocked(vscode.workspace.getConfiguration).mockReturnValueOnce(
        createFakeWorkspaceConfiguration({
          enabledSupportedLanguages: {
            c: false,
            cpp: true,
            python: false,
          },
        }),
      );

      const actual = getAIAssistedCodeSuggestionsLanguages();
      expect(actual).toContain('cpp');
      expect(actual).not.toContain('c');
      expect(actual).not.toContain('python');
    });

    it('should not return languages both enabled and disabled by the user', () => {
      jest.mocked(vscode.workspace.getConfiguration).mockReturnValueOnce(
        createFakeWorkspaceConfiguration({
          additionalLanguages: ['python'],
          enabledSupportedLanguages: {
            python: false,
          },
        }),
      );

      expect(getAIAssistedCodeSuggestionsLanguages()).not.toContain('python');
    });
  });
});
