import * as vscode from 'vscode';
import { CONFIG_NAMESPACE } from '../constants';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE,
  AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES,
} from '../code_suggestions/constants';
import { CustomQuery } from '../gitlab/custom_query';
import { FeatureFlag } from '../feature_flags/constants';
import { isArrayOfString, isBoolean, isRecordOfStringBoolean, isString } from './type_predicates';
import { getConfigurationTargetForKey } from './get_configuration_target_for_key';

// These constants represent `settings.json` keys. Other constants belong to `constants.ts`.
export const GITLAB_DEBUG_MODE = 'gitlab.debug';
export const AI_ASSISTED_CODE_SUGGESTIONS_MODE = 'gitlab.aiAssistedCodeSuggestions.enabled';
export const AI_ASSISTED_CODE_SUGGESTIONS_SUPPORTED_LANGUAGES =
  'gitlab.aiAssistedCodeSuggestions.enabledSupportedLanguages';
export const AI_ASSISTED_CODE_SUGGESTIONS_USER_LANGUAGES =
  'gitlab.aiAssistedCodeSuggestions.additionalLanguages';

export interface ExtensionConfiguration {
  pipelineGitRemoteName?: string;
  debug: boolean;
  /** this is an undocumented setting to help us test telemetry changes by pointing them to local snowplow instance */
  trackingUrl: string;
  ignoreCertificateErrors: boolean;
  featureFlags: Partial<Record<FeatureFlag, boolean>>;
  customQueries: CustomQuery[];
}

export interface AiAssistedCodeSuggestionsConfiguration {
  enabled: boolean;
  preferredAccount?: string; // Optional as default value is null
  suggestionsCache?: object; // Optional as does not define contributes.configuration
  additionalLanguages: string[];
  enabledSupportedLanguages: Record<string, boolean>;
  openTabsContext: boolean;
}

export interface httpAgentConfiguration {
  ca?: string;
  cert?: string;
  certKey?: string;
}

// VS Code returns a value or `null` but undefined is better for using default function arguments
const turnNullToUndefined = <T>(val: T | null | undefined): T | undefined => val ?? undefined;

const booleanOrDefault = (val: unknown, defaultValue: boolean): boolean =>
  isBoolean(val) ? val : defaultValue;

export function getExtensionConfiguration(): ExtensionConfiguration {
  const workspaceConfig = vscode.workspace.getConfiguration(CONFIG_NAMESPACE);

  return {
    pipelineGitRemoteName: turnNullToUndefined(workspaceConfig?.pipelineGitRemoteName),
    featureFlags: workspaceConfig?.featureFlags ?? {},
    debug: workspaceConfig?.debug || false,
    ignoreCertificateErrors: workspaceConfig?.ignoreCertificateErrors || false,
    customQueries: workspaceConfig?.customQueries || [],
    trackingUrl: turnNullToUndefined(workspaceConfig?.trackingUrl),
  };
}

function getAdditionalLanguages(
  config: vscode.WorkspaceConfiguration,
): AiAssistedCodeSuggestionsConfiguration['additionalLanguages'] {
  const value = config.get('additionalLanguages');
  return isArrayOfString(value) ? value : [];
}

function getEnabledSupportedLanguages(
  config: vscode.WorkspaceConfiguration,
): AiAssistedCodeSuggestionsConfiguration['enabledSupportedLanguages'] {
  const value = config.get('enabledSupportedLanguages');
  return isRecordOfStringBoolean(value) ? value : {};
}

export function getAiAssistedCodeSuggestionsConfiguration(): AiAssistedCodeSuggestionsConfiguration {
  const aiAssistedCodeSuggestionsConfig = vscode.workspace.getConfiguration(
    AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE,
  );

  const { preferredAccount } = aiAssistedCodeSuggestionsConfig;

  return {
    enabled: booleanOrDefault(aiAssistedCodeSuggestionsConfig.enabled, true),
    preferredAccount: isString(preferredAccount) ? preferredAccount : undefined,
    suggestionsCache: aiAssistedCodeSuggestionsConfig.suggestionsCache,
    additionalLanguages: getAdditionalLanguages(aiAssistedCodeSuggestionsConfig),
    enabledSupportedLanguages: getEnabledSupportedLanguages(aiAssistedCodeSuggestionsConfig),
    openTabsContext: booleanOrDefault(aiAssistedCodeSuggestionsConfig.openTabsContext, true),
  };
}

export function getHttpAgentConfiguration(): httpAgentConfiguration {
  const workspaceConfig = vscode.workspace.getConfiguration(CONFIG_NAMESPACE);
  return {
    ca: workspaceConfig.ca,
    cert: workspaceConfig.cert,
    certKey: workspaceConfig.certKey,
  };
}

/**
 * Calls `config.update` but uses `getConfigurationTargetForKey` to make
 * sure we pick the right ConfigurationTarget
 */
const updateConfig = <T>(config: vscode.WorkspaceConfiguration, key: string, value: T) => {
  const target = getConfigurationTargetForKey(config, key);
  return config.update(key, value, target);
};

/**
 * Set 'gitlab.aiAssistedCodeSuggestions' configuration values from the given object.
 *
 * To remove a configuration key, use `undefined` as the value. This behavior
 * comes from the underlying `WorkspaceConfiguration#update` method.
 *
 * To avoid unnecessary writes to user and/or workspace configuration files,
 * only provide object properties for settings that will actually change. In
 * other words:
 *
 *     // Bad
 *     setAiAssistedCodeSuggestionsConfiguration({
 *       ...getAiAssistedCodeSuggestionsConfiguration(),
 *       foo,
 *       bar,
 *     })
 *
 *     // Good
 *     setAiAssistedCodeSuggestionsConfiguration({
 *       foo,
 *       bar,
 *     })
 */
export async function setAiAssistedCodeSuggestionsConfiguration(
  config: Partial<AiAssistedCodeSuggestionsConfiguration>,
) {
  const aiAssistedCodeSuggestionsConfig = vscode.workspace.getConfiguration(
    AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE,
  );

  for (const [key, value] of Object.entries(config)) {
    // eslint-disable-next-line no-await-in-loop
    await updateConfig(aiAssistedCodeSuggestionsConfig, key, value);
  }
}

/**
 * Transform the given `enabledSupportedLanguages` configuration value into an
 * array of disabled, supported languages. This distinction is necessary as the
 * setting is presented as a checklist of enabled supported languages, but the
 * language server expects a list of disabled languages.
 *
 * See https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/issues/174
 * for more details.
 */
export const parseDisabledSupportedLanguages = (
  enabledSupportedLanguages: AiAssistedCodeSuggestionsConfiguration['enabledSupportedLanguages'],
): string[] =>
  Object.entries(enabledSupportedLanguages).reduce((acc: string[], [languageId, isEnabled]) => {
    if (!isEnabled) acc.push(languageId);

    return acc;
  }, []);

export const getAIAssistedCodeSuggestionsLanguages = () => {
  const { additionalLanguages: enabledUnsupported, enabledSupportedLanguages } =
    getAiAssistedCodeSuggestionsConfiguration();
  const disabledSupported = parseDisabledSupportedLanguages(enabledSupportedLanguages);

  const languages = new Set(AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES);

  for (const language of enabledUnsupported) {
    languages.add(language);
  }

  for (const language of disabledSupported) {
    languages.delete(language);
  }

  return Array.from(languages);
};
