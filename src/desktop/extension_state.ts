import * as vscode from 'vscode';
import { GitLabPlatformManager } from '../common/platform/gitlab_platform';
import { log } from '../common/log';
import { AccountService } from './accounts/account_service';
import { gitExtensionWrapper } from './git/git_extension_wrapper';

const openRepositoryCount = (): number => gitExtensionWrapper.gitRepositories.length;
export class ExtensionState {
  #changeValidEmitter = new vscode.EventEmitter<void>();

  onDidChangeValid = this.#changeValidEmitter.event;

  accountService?: AccountService;

  manager?: GitLabPlatformManager;

  #lastValid = false;

  async init(accountService: AccountService, manager?: GitLabPlatformManager): Promise<void> {
    this.accountService = accountService;
    this.manager = manager;
    this.#lastValid = this.isValid();
    accountService.onDidChange(this.updateExtensionStatus, this);
    gitExtensionWrapper.onRepositoryCountChanged(this.updateExtensionStatus, this);
    await this.updateExtensionStatus();
  }

  #hasAnyAccounts(): boolean {
    if (!this.accountService) {
      const error = new Error('ExtensionState has not been initialized.');
      log.error('we call account service, but it is not present', error);
      throw error;
    }
    return this.accountService.getAllAccounts().length > 0;
  }

  isValid(): boolean {
    return this.#hasAnyAccounts() && openRepositoryCount() > 0;
  }

  async updateExtensionStatus(): Promise<void> {
    await vscode.commands.executeCommand('setContext', 'gitlab:noAccount', !this.#hasAnyAccounts());
    await vscode.commands.executeCommand(
      'setContext',
      'gitlab:openRepositoryCount',
      openRepositoryCount(),
    );

    await vscode.commands.executeCommand('setContext', 'gitlab:validState', this.isValid());
    if (this.#lastValid !== this.isValid()) {
      this.#lastValid = this.isValid();
      this.#changeValidEmitter.fire();
    }
  }
}

export const extensionState = new ExtensionState();
